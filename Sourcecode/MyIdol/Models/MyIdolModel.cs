using Microsoft.AspNetCore.Identity;
using System; 
using System.ComponentModel.DataAnnotations; 
 
namespace MyIdol.Models {
    public class NewsUser :  IdentityUser{         
        public string  FirstName { get; set;}         
        public string  LastName { get; set;}     
    }
    public class MyIdolCategory {  
        public int MyIdolCategoryID { get; set;} 
        public string ShortName {get; set;} 
        public string FullName { get; set;}   
    }
    public class MyIdolEvent { 
        public int  MyIdolEventID { get; set; } 
 
        public int MyIdolCategoryID { get; set; } 
        public MyIdolCategory  MyIdolCat { get; set; } 
 
        [DataType(DataType.Date)]        
        public string ReportDate { get; set; }
        public string MyIdolDetail { get; set; }         
        public string Time { get; set; }         
        public string Place { get; set; }  

        public string NewsUserId {get; set;}         
        public NewsUser  postUser {get; set;} 
    }    
}

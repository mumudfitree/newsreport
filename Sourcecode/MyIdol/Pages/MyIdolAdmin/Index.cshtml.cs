using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using MyIdol.Models;
using MyIdol.Data;

namespace MyIdol.Pages.MyIdolAdmin
{
    public class IndexModel : PageModel
    {
        private readonly MyIdol.Data.MyldolContext _context;

        public IndexModel(MyIdol.Data.MyldolContext context)
        {
            _context = context;
        }

        public IList<MyIdolEvent> MyIdolEvent { get;set; }

        public async Task OnGetAsync()
        {
            MyIdolEvent = await _context.MyIdolList
                .Include(m => m.MyIdolCat).ToListAsync();
        }
    }
}

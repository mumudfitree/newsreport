using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using MyIdol.Models;
using MyIdol.Data;

namespace MyIdol.Pages.MyIdolAdmin
{
    public class EditModel : PageModel
    {
        private readonly MyIdol.Data.MyldolContext _context;

        public EditModel(MyIdol.Data.MyldolContext context)
        {
            _context = context;
        }

        [BindProperty]
        public MyIdolEvent MyIdolEvent { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            MyIdolEvent = await _context.MyIdolList
                .Include(m => m.MyIdolCat).FirstOrDefaultAsync(m => m.MyIdolEventID == id);

            if (MyIdolEvent == null)
            {
                return NotFound();
            }
           ViewData["MyIdolCategoryID"] = new SelectList(_context.MyIdolCategory, "MyIdolCategoryID", "MyIdolCategoryID");
            return Page();
        }

        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            _context.Attach(MyIdolEvent).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!MyIdolEventExists(MyIdolEvent.MyIdolEventID))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return RedirectToPage("./Index");
        }

        private bool MyIdolEventExists(int id)
        {
            return _context.MyIdolList.Any(e => e.MyIdolEventID == id);
        }
    }
}

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using MyIdol.Models;
using MyIdol.Data;

namespace MyIdol.Pages.MyIdolAdmin
{
    public class CreateModel : PageModel
    {
        private readonly MyIdol.Data.MyldolContext _context;

        public CreateModel(MyIdol.Data.MyldolContext context)
        {
            _context = context;
        }

        public IActionResult OnGet()
        {
        ViewData["MyIdolCategoryID"] = new SelectList(_context.MyIdolCategory, "MyIdolCategoryID", "ShortName");
            return Page();
        }

        [BindProperty]
        public MyIdolEvent MyIdolEvent { get; set; }

        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            _context.MyIdolList.Add(MyIdolEvent);
            await _context.SaveChangesAsync();

            return RedirectToPage("./Index");
        }
    }
}
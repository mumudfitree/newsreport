using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using MyIdol.Models;
using MyIdol.Data;

namespace MyIdol.Pages.MyIdolAdmin
{
    public class DetailsModel : PageModel
    {
        private readonly MyIdol.Data.MyldolContext _context;

        public DetailsModel(MyIdol.Data.MyldolContext context)
        {
            _context = context;
        }

        public MyIdolEvent MyIdolEvent { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            MyIdolEvent = await _context.MyIdolList
                .Include(m => m.MyIdolCat).FirstOrDefaultAsync(m => m.MyIdolEventID == id);

            if (MyIdolEvent == null)
            {
                return NotFound();
            }
            return Page();
        }
    }
}

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using MyIdol.Models;
using MyIdol.Data;

namespace MyIdol.Pages.MyIdolcategoryAdmin
{
    public class DetailsModel : PageModel
    {
        private readonly MyIdol.Data.MyldolContext _context;

        public DetailsModel(MyIdol.Data.MyldolContext context)
        {
            _context = context;
        }

        public MyIdolCategory MyIdolCategory { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            MyIdolCategory = await _context.MyIdolCategory.FirstOrDefaultAsync(m => m.MyIdolCategoryID == id);

            if (MyIdolCategory == null)
            {
                return NotFound();
            }
            return Page();
        }
    }
}

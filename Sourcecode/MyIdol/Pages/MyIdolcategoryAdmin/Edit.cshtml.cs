using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using MyIdol.Models;
using MyIdol.Data;

namespace MyIdol.Pages.MyIdolcategoryAdmin
{
    public class EditModel : PageModel
    {
        private readonly MyIdol.Data.MyldolContext _context;

        public EditModel(MyIdol.Data.MyldolContext context)
        {
            _context = context;
        }

        [BindProperty]
        public MyIdolCategory MyIdolCategory { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            MyIdolCategory = await _context.MyIdolCategory.FirstOrDefaultAsync(m => m.MyIdolCategoryID == id);

            if (MyIdolCategory == null)
            {
                return NotFound();
            }
            return Page();
        }

        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            _context.Attach(MyIdolCategory).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!MyIdolCategoryExists(MyIdolCategory.MyIdolCategoryID))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return RedirectToPage("./Index");
        }

        private bool MyIdolCategoryExists(int id)
        {
            return _context.MyIdolCategory.Any(e => e.MyIdolCategoryID == id);
        }
    }
}

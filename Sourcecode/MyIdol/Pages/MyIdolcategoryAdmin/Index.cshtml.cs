using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using MyIdol.Models;
using MyIdol.Data;

namespace MyIdol.Pages.MyIdolcategoryAdmin
{
    public class IndexModel : PageModel
    {
        private readonly MyIdol.Data.MyldolContext _context;

        public IndexModel(MyIdol.Data.MyldolContext context)
        {
            _context = context;
        }

        public IList<MyIdolCategory> MyIdolCategory { get;set; }

        public async Task OnGetAsync()
        {
            MyIdolCategory = await _context.MyIdolCategory.ToListAsync();
        }
    }
}

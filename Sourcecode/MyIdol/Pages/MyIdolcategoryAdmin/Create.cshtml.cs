using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using MyIdol.Models;
using MyIdol.Data;

namespace MyIdol.Pages.MyIdolcategoryAdmin
{
    public class CreateModel : PageModel
    {
        private readonly MyIdol.Data.MyldolContext _context;

        public CreateModel(MyIdol.Data.MyldolContext context)
        {
            _context = context;
        }

        public IActionResult OnGet()
        {
            return Page();
        }

        [BindProperty]
        public MyIdolCategory MyIdolCategory { get; set; }

        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            _context.MyIdolCategory.Add(MyIdolCategory);
            await _context.SaveChangesAsync();

            return RedirectToPage("./Index");
        }
    }
}
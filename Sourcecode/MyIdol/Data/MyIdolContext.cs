using MyIdol.Models; 
using Microsoft.EntityFrameworkCore; 
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
namespace MyIdol.Data {     
    public class MyldolContext : IdentityDbContext<NewsUser>     
    {   public DbSet<MyIdolEvent> MyIdolList { get; set; }         
        public DbSet<MyIdolCategory> MyIdolCategory { get; set; }      

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)        
        {             
            optionsBuilder.UseSqlite(@"Data source= MyIdol.db");                  
        }     
    } 
}